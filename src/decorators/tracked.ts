import {safeSetState} from '../functions/safe-set-state';

let uniqueCounter = 0;

export function tracked(target: any, propertyKey: string): any
{
    const desc: PropertyDescriptor = Object.getOwnPropertyDescriptor(target, propertyKey) || {
        configurable: false
    };

    desc.get = function(this: { [key: string]: any }) {
        return this.state[propertyKey];
    };
    desc.set = function(this: any, value: any) {
        if (this.state[propertyKey] === value)
            return;
        
        this.state[propertyKey] = value;
        
        // This may trigger a synchronous render(), so call this AFTER _currentStateValues has been updated
        safeSetState(this, { [propertyKey]: value, '$$forceUpdate': uniqueCounter++ });
    };

    Object.defineProperty(target, propertyKey, desc);
}