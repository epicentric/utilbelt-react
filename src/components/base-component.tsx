import {Subject, Subscription, from, ObservableInput} from 'rxjs';
import {safeSetState} from '../functions/safe-set-state';
import {Component} from 'react';
// @ts-ignore
import {shouldComponentUpdate} from 'reflective-bind';
import {take} from 'rxjs/operators';

interface ChangeParam<TProps, TState, TSnapshotState>
{
    prevProps: TProps;
    prevState: TState;
    props: TProps;
    state: TState;
    didMount: boolean;
    snapshot?: TSnapshotState;
}

export abstract class BaseComponent<TProps = {}, TState = {}, TSnapshotState = {}> extends Component<TProps, TState, TSnapshotState>
{
    public readonly isPureComponent = true;
    public state: TState = {} as TState;
    public isMounted!: boolean;

    public didChange: Subject<ChangeParam<TProps, TState, TSnapshotState>> = new Subject;
    
    protected subscriptions: Map<string, Subscription> = new Map();
    public didUpdate: Subject<ChangeParam<TProps, TState, TSnapshotState>> = new Subject;
    protected willUnmount: Subject<boolean> = new Subject;

    public constructor(props: TProps, context: any)
    {
        super(props, context);
        // nix built in deprecated warning throwing property with our own
        Object.defineProperty(this, 'isMounted', { writable: true, value: false });
    }

    public shouldComponentUpdate(nextProps: Readonly<TProps>, nextState: Readonly<TState>, nextContext: any): boolean
    {
        return shouldComponentUpdate(this, nextProps, nextState, nextContext);
    }

    protected addObservable<T>(key: string, obs: ObservableInput<T>, defaultValue?: T): void
    {
        this.removeObservable(key);

        if (undefined !== defaultValue)
            safeSetState(this, { [key]: defaultValue } as any);

        this.subscriptions.set(key, from(obs).subscribe({
            next: value => {
                safeSetState<TProps, TState, TSnapshotState>(this as any, { [key]: value } as any);
            },
            error: error => {
                console.error(error);
                this.removeObservable(key);
            },
            complete: () => {
                console.log(`'${key}' observable completed`);
                this.removeObservable(key)
            }
        }));
    }
    
    protected removeObservable(key: string): void
    {
        const prevSubscription = this.subscriptions.get(key);

        if (prevSubscription)
            prevSubscription.unsubscribe();
    }

    protected clearObservables(): void
    {
        for (const value of this.subscriptions.values())
        {
            value.unsubscribe();
        }

        this.setState(Array.from(this.subscriptions.keys()).reduce((acc, keyName) => {
            acc[keyName] = null;
            return acc;
        }, {} as any));

        this.subscriptions.clear();
    }

    public componentDidUpdate(prevProps: Readonly<TProps>, prevState: Readonly<TState>, snapshot: Readonly<TSnapshotState>): void
    {
        this.didUpdate.next({
            props: this.props,
            state: this.state,
            prevProps,
            prevState,
            didMount: false,
            snapshot,
        });
        
        this.triggerDidChange(false, prevProps, prevState, snapshot);
    }

    public componentWillUnmount(): void
    {
        this.isMounted = false;

        for (const subsciption of this.subscriptions.values())
        {
            subsciption.unsubscribe();
        }

        this.subscriptions.clear();
        this.willUnmount.next(true);
        
        // Cleanup event handlers
        this.didChange.complete();
        this.didChange.unsubscribe();
        this.didUpdate.complete();
        this.didUpdate.unsubscribe();
        this.willUnmount.complete();
        this.willUnmount.unsubscribe();
    }

    public componentDidMount(): void
    {
        this.isMounted = true;
        
        this.triggerDidChange(true);
    }
    
    protected triggerDidChange(didMount: boolean, prevProps?: TProps, prevState?: TState, snapshot?: TSnapshotState): void
    {
        this.didChange.next({
            prevProps: prevProps || ({} as any),
            prevState: prevState || ({} as any),
            props: this.props,
            state: this.state,
            didMount,
            snapshot
        });
    }
    
    protected waitForUpdate(): Promise<any>
    {
        return this.didChange.pipe(take(1)).toPromise();
    }
}